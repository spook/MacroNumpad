﻿using MacroNumpad.Common.Types;
using MacroNumpad.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.Common.Extensions
{
    public static class EnumExtensions
    {
        private static ResourceManager resourceManager = new ResourceManager(typeof(Strings));

        public static string GetLocalizedName(this object value)
        {
            FieldInfo field = value.GetType().GetField(value.ToString());
            if (field != null)
            {
                var attributes = (EnumResourceAttribute[])field.GetCustomAttributes(typeof(EnumResourceAttribute), false);
                if ((attributes.Length > 0) && (!string.IsNullOrEmpty(attributes[0].Key)))
                    return resourceManager.GetString(attributes[0].Key);
                else
                    value.ToString();
            }

            return null;
        }
    }
}
