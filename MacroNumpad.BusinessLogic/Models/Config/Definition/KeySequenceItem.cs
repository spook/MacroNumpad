﻿using MacroNumpad.BusinessLogic.Types;

namespace MacroNumpad.BusinessLogic.Models.Config.Definition
{
    public class KeySequenceItem
    {
        public Keys Key { get; set; }
        public KeyActionType ActionType { get; set; }
    }
}