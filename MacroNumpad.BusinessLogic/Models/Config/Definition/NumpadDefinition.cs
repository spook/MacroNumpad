﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Models.Config.Definition
{
    public class NumpadDefinition
    {
        public List<NumpadScreen> Screens { get; set; }
    }
}
