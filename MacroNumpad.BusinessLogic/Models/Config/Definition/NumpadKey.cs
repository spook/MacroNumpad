﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Xml.Serialization;

namespace MacroNumpad.BusinessLogic.Models.Config.Definition
{
    public class NumpadKey
    {
        public string Title { get; set; }

        [XmlIgnore]
        public Bitmap Icon { get; set; }

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        [XmlElement("Icon")]
        public byte[] IconSerialized
        {
            get
            { 
                if (Icon == null) return null;
                using (MemoryStream ms = new MemoryStream())
                {
                    Icon.Save(ms, ImageFormat.Png);
                    return ms.ToArray();
                }
            }
            set
            {
                if (value == null)
                {
                    Icon = null;
                }
                else
                {
                    using (MemoryStream ms = new MemoryStream(value))
                    {
                        Icon = new Bitmap(ms);
                    }
                }
            }
        }

        [XmlArray]
        [XmlArrayItem(ElementName = "SwitchToScreen", Type = typeof(SwitchScreenNumpadAction))]
        [XmlArrayItem(ElementName = "KeySequence", Type = typeof(KeySequenceNumpadAction))]
        [XmlArrayItem(ElementName = "Execute", Type = typeof(ExecuteNumpadAction))]
        [XmlArrayItem(ElementName = "BringToFront", Type = typeof(BringToFrontNumpadAction))]
        public List<NumpadAction> Actions { get; set; }
    }
}