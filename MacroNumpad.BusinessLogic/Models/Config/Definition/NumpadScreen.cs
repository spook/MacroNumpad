﻿using System.Collections.Generic;

namespace MacroNumpad.BusinessLogic.Models.Config.Definition
{
    public class NumpadScreen
    {
        public int Id { get; set; }
        public string Header { get; set; }

        /// <summary>
        /// Keys in this list are kept in the same order as
        /// in NumpadKeys enumeration.
        /// </summary>
        public List<NumpadKey> Keys { get; set; }
    }
}