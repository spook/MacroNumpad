﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Models.Config.Definition
{
    public class BringToFrontNumpadAction : NumpadAction
    {
        public string Executable { get; set; }
        public string Path { get; set; }
    }
}
