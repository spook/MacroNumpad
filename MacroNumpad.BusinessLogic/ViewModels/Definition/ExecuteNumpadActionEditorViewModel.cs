﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;

namespace MacroNumpad.BusinessLogic.ViewModels.Definition
{
    public class ExecuteNumpadActionEditorViewModel : NumpadKeyActionEditorViewModel
    {
        private string path;

        public ExecuteNumpadActionEditorViewModel(ExecuteNumpadAction executeNumpadAction)
        {
            path = executeNumpadAction.Path;
        }

        public ExecuteNumpadActionEditorViewModel()
        {
            path = null;
        }

        public override NumpadAction BuildModel()
        {
            var result = new ExecuteNumpadAction();
            result.Path = path;
            return result;
        }

        public string Path
        {
            get => path;
            set => Set(ref path, value);
        }
    }
}