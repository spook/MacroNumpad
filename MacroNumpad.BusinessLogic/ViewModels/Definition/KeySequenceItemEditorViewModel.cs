﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using MacroNumpad.BusinessLogic.Types;
using MacroNumpad.BusinessLogic.ViewModels.Base;
using System;

namespace MacroNumpad.BusinessLogic.ViewModels.Definition
{
    public class KeySequenceItemEditorViewModel : BaseViewModel
    {
        private KeyActionType actionType;
        private Keys key;

        public KeySequenceItemEditorViewModel(KeySequenceItem keySequenceItem)
        {
            actionType = keySequenceItem.ActionType;
            key = keySequenceItem.Key;
        }

        public KeySequenceItemEditorViewModel()
        {
            actionType = KeyActionType.Press;
            key = Keys.Enter;
        }

        public KeyActionType ActionType
        {
            get => actionType;
            set => Set(ref actionType, value);
        }

        public Keys Key
        {
            get => key;
            set => Set(ref key, value);
        }

        public KeySequenceItem BuildModel()
        {
            var result = new KeySequenceItem();

            result.Key = key;
            result.ActionType = actionType;

            return result;
        }
    }
}