﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using MacroNumpad.BusinessLogic.Services.Dialogs;
using MacroNumpad.BusinessLogic.Services.Messaging;
using MacroNumpad.BusinessLogic.Types;
using MacroNumpad.BusinessLogic.ViewModels.Base;
using MacroNumpad.Resources;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace MacroNumpad.BusinessLogic.ViewModels.Definition
{
    public class NumpadScreenEditorViewModel : BaseViewModel, INumpadKeyEditorHandler
    {
        private int id;
        private string header;
        private NumpadKeyEditorViewModel selectedKey;
        private readonly IDialogService dialogService;
        private readonly IMessagingService messagingService;

        void INumpadKeyEditorHandler.NotifySelected(NumpadKeyEditorViewModel viewModel)
        {
            SelectedKey = viewModel;
        }

        public NumpadScreenEditorViewModel(NumpadScreen screen, IDialogService dialogService, IMessagingService messagingService)
        {
            this.id = screen.Id;
            this.header = screen.Header;

            this.dialogService = dialogService;
            this.messagingService = messagingService;

            Keys = new ObservableCollection<NumpadKeyEditorViewModel>();
            if (screen.Keys.Count > 0)
            {
                for (int i = 0; i < screen.Keys.Count; i++)
                {
                    var keyViewModel = new NumpadKeyEditorViewModel(screen.Keys[i], this, dialogService, messagingService);
                    Keys.Add(keyViewModel);
                }

                while (Keys.Count < Enum.GetValues(typeof(NumpadKeys)).Length)
                    Keys.Add(new NumpadKeyEditorViewModel(this, dialogService, messagingService));
            }
        }

        public NumpadScreenEditorViewModel(int newId, IDialogService dialogService, IMessagingService messagingService)
        {
            this.id = newId;
            this.header = String.Format(Strings.Screen_TitleTemplate, newId);
            this.dialogService = dialogService;
            this.messagingService = messagingService;

            Keys = new ObservableCollection<NumpadKeyEditorViewModel>();
            for (int i = 0; i < Enum.GetValues(typeof(NumpadKeys)).Length; i++)
                Keys.Add(new NumpadKeyEditorViewModel(this, dialogService, messagingService));
        }

        public NumpadScreen BuildModel()
        {
            var result = new NumpadScreen();
            result.Id = this.Id;
            result.Header = this.Header;
            result.Keys = Keys.Select(k => k.BuildModel()).ToList();

            return result;
        }

        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        public string Header
        {
            get => header;
            set => Set(ref header, value);
        }

        public ObservableCollection<NumpadKeyEditorViewModel> Keys { get; }

        public NumpadKeyEditorViewModel SelectedKey
        {
            get => selectedKey;
            set => Set(ref selectedKey, value);
        }
    }
}