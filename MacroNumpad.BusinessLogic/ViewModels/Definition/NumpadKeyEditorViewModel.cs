﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using MacroNumpad.BusinessLogic.Services.Dialogs;
using MacroNumpad.BusinessLogic.Services.Messaging;
using MacroNumpad.BusinessLogic.ViewModels.Base;
using MacroNumpad.Common.Wpf.Helpers;
using MacroNumpad.Resources;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;

namespace MacroNumpad.BusinessLogic.ViewModels.Definition
{
    public class NumpadKeyEditorViewModel : BaseViewModel
    {
        private readonly INumpadKeyEditorHandler handler;
        private readonly IDialogService dialogService;
        private readonly IMessagingService messagingService;
        private Bitmap icon;
        private ImageSource iconSource;
        private string title;
        private bool selected;
        private NumpadKeyActionEditorViewModel selectedAction;
        private int selectedActionIndex;

        private void HandleSelectedChanged()
        {
            if (selected)
                handler.NotifySelected(this);
        }

        private void DoChooseIcon()
        {
            (bool result, string path) = dialogService.ShowOpenDialog(Strings.Filter_Icons);
            if (result)
            {
                try
                {
                    Bitmap targetBitmap = new Bitmap(32, 32, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                    using (Bitmap newIcon = new Bitmap(path))
                    using (Graphics g = Graphics.FromImage(targetBitmap))
                        g.DrawImage(newIcon, 0, 0, targetBitmap.Width, targetBitmap.Height);

                    icon?.Dispose();
                    icon = targetBitmap;
                    iconSource = ImageSourceHelper.FromBitmap(icon);
                }
                catch (Exception e)
                {
                    icon = null;
                    iconSource = null;

                    messagingService.ShowError(String.Format(Strings.Error_FailedToOpenImage, e.Message));
                }

                OnPropertyChanged(nameof(IconSource));
            }
        }

        private void DoClearIcon()
        {
            icon = null;
            iconSource = null;

            OnPropertyChanged(nameof(IconSource));
        }

        private void DoDeleteAction()
        {
            if (messagingService.AskYesNo(Strings.Message_ActionDeleteConfirmation))
            {
                int actionIndex = Actions.IndexOf(SelectedAction);

                Actions.RemoveAt(actionIndex);
                actionIndex = Math.Max(0, Math.Min(Actions.Count - 1, actionIndex));
                if (Actions.Any())
                    SelectedAction = Actions[actionIndex];
            }
        }

        private void DoMoveActionDown()
        {
            Actions.Move(SelectedActionIndex, SelectedActionIndex + 1);
        }

        private void DoMoveActionUp()
        {
            Actions.Move(SelectedActionIndex, SelectedActionIndex - 1);
        }

        private void DoAddSwitchToScreen()
        {
            var newAction = new SwitchScreenNumpadActionEditorViewModel();
            Actions.Add(newAction);
            SelectedAction = newAction;
        }

        private void DoAddBringToFront()
        {
            var newAction = new BringToFrontNumpadActionEditorViewModel();
            Actions.Add(newAction);
            SelectedAction = newAction;
        }

        private void DoAddExecute()
        {
            var newAction = new ExecuteNumpadActionEditorViewModel();
            Actions.Add(newAction);
            SelectedAction = newAction;
        }

        private void DoAddKeySequence()
        {
            var newAction = new KeySequenceNumpadActionEditorViewModel(messagingService);
            Actions.Add(newAction);
            SelectedAction = newAction;
        }

        private void InitializeCommands()
        {
            ChooseIconCommand = new AppCommand(obj => DoChooseIcon());
            ClearIconCommand = new AppCommand(obj => DoClearIcon());

            AddKeySequenceActionCommand = new AppCommand(obj => DoAddKeySequence());
            AddExecuteActionCommand = new AppCommand(obj => DoAddExecute());
            AddBringToFrontActionCommand = new AppCommand(obj => DoAddBringToFront());
            AddSwitchToScreenCommand = new AppCommand(obj => DoAddSwitchToScreen());

            var firstItemSelectedCondition = new LambdaCondition<NumpadKeyEditorViewModel>(this, vm => vm.SelectedActionIndex == 0, false);
            var lastItemSelectedCondition = new LambdaCondition<NumpadKeyEditorViewModel>(this, vm => vm.SelectedActionIndex == vm.Actions.Count - 1, false);
            var anyItemSelectedCondition = new LambdaCondition<NumpadKeyEditorViewModel>(this, vm => vm.SelectedAction != null, false);

            MoveActionUpCommand = new AppCommand(obj => DoMoveActionUp(), anyItemSelectedCondition & !firstItemSelectedCondition);
            MoveActionDownCommand = new AppCommand(obj => DoMoveActionDown(), anyItemSelectedCondition & !lastItemSelectedCondition);
            DeleteActionCommand = new AppCommand(obj => DoDeleteAction(), anyItemSelectedCondition);
        }

        public NumpadKeyEditorViewModel(NumpadKey numpadKey, 
            INumpadKeyEditorHandler handler, 
            IDialogService dialogService, 
            IMessagingService messagingService)
        {
            this.handler = handler;
            this.dialogService = dialogService;
            this.messagingService = messagingService;

            icon = numpadKey.Icon;
            iconSource = ImageSourceHelper.FromBitmap(icon);
            title = numpadKey.Title;

            Actions = new ObservableCollection<NumpadKeyActionEditorViewModel>();
            if (numpadKey.Actions != null && numpadKey.Actions.Count > 0)
            {
                for (int i = 0; i < numpadKey.Actions.Count; i++)
                {
                    var action = NumpadKeyActionEditorViewModel.From(numpadKey.Actions[i], messagingService);
                    Actions.Add(action);
                }
            }

            InitializeCommands();
        }

        public NumpadKeyEditorViewModel(INumpadKeyEditorHandler handler,
            IDialogService dialogService,
            IMessagingService messagingService)
        {
            this.handler = handler;
            this.dialogService = dialogService;
            this.messagingService = messagingService;

            icon = null;
            iconSource = null;
            title = null;            

            Actions = new ObservableCollection<NumpadKeyActionEditorViewModel>();

            InitializeCommands();
        }

        public NumpadKey BuildModel()
        {
            var result = new NumpadKey();
            result.Icon = icon;
            result.Title = title;
            result.Actions = Actions.Select(a => a.BuildModel()).ToList();

            return result;
        }

        // Public properties --------------------------------------------------

        public ImageSource IconSource => iconSource;

        public string Title
        {
            get => title;
            set => Set(ref title, value);
        }

        public ObservableCollection<NumpadKeyActionEditorViewModel> Actions { get; }

        public bool Selected
        {
            get => selected;
            set => Set(ref selected, value, changeHandler: HandleSelectedChanged);
        }

        public NumpadKeyActionEditorViewModel SelectedAction
        {
            get => selectedAction;
            set => Set(ref selectedAction, value);
        }

        public int SelectedActionIndex
        {
            get => selectedActionIndex;
            set => Set(ref selectedActionIndex, value);
        }

        public ICommand ChooseIconCommand { get; private set; }
        public ICommand ClearIconCommand { get; private set; }
        public ICommand AddKeySequenceActionCommand { get; private set; }
        public ICommand AddExecuteActionCommand { get; private set; }
        public ICommand AddBringToFrontActionCommand { get; private set; }
        public ICommand AddSwitchToScreenCommand { get; private set; }
        public ICommand MoveActionUpCommand { get; private set; }
        public ICommand MoveActionDownCommand { get; private set; }
        public ICommand DeleteActionCommand { get; private set; }
    }
}