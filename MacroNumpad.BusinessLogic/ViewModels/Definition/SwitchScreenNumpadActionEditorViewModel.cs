﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;

namespace MacroNumpad.BusinessLogic.ViewModels.Definition
{
    public class SwitchScreenNumpadActionEditorViewModel : NumpadKeyActionEditorViewModel
    {
        private int targetId;

        public SwitchScreenNumpadActionEditorViewModel(SwitchScreenNumpadAction switchScreenAction)
        {
            targetId = switchScreenAction.TargetId;
        }

        public SwitchScreenNumpadActionEditorViewModel()
        {
            targetId = 0;
        }

        public override NumpadAction BuildModel()
        {
            var result = new SwitchScreenNumpadAction();
            result.TargetId = targetId;
            return result;
        }

        public int TargetId
        {
            get => targetId;
            set => Set(ref targetId, value);
        }
    }
}