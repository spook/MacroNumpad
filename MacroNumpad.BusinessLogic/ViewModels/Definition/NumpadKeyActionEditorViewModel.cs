﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using MacroNumpad.BusinessLogic.Services.Messaging;
using MacroNumpad.BusinessLogic.ViewModels.Base;
using System;

namespace MacroNumpad.BusinessLogic.ViewModels.Definition
{
    public abstract class NumpadKeyActionEditorViewModel : BaseViewModel
    {
        public static NumpadKeyActionEditorViewModel From(NumpadAction numpadAction, IMessagingService messagingService)
        {
            if (numpadAction is KeySequenceNumpadAction keySequenceAction)
            {
                return new KeySequenceNumpadActionEditorViewModel(keySequenceAction, messagingService);
            }
            else if (numpadAction is ExecuteNumpadAction executeAction)
            {
                return new ExecuteNumpadActionEditorViewModel(executeAction);
            }
            else if (numpadAction is BringToFrontNumpadAction bringToFrontAction)
            {
                return new BringToFrontNumpadActionEditorViewModel(bringToFrontAction);
            }
            else if (numpadAction is SwitchScreenNumpadAction switchScreenAction)
            {
                return new SwitchScreenNumpadActionEditorViewModel(switchScreenAction);
            }
            else
                throw new InvalidOperationException("Unsupported numpad action!");
        }

        public abstract NumpadAction BuildModel();        
    }
}