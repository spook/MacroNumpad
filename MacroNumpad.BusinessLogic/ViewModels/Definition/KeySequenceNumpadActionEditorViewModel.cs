﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using MacroNumpad.BusinessLogic.Services.Messaging;
using MacroNumpad.Resources;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace MacroNumpad.BusinessLogic.ViewModels.Definition
{
    public class KeySequenceNumpadActionEditorViewModel : NumpadKeyActionEditorViewModel
    {
        private readonly IMessagingService messagingService;
        private KeySequenceItemEditorViewModel selectedItem;
        private int selectedItemIndex;

        private void DoDeleteItem()
        {
            if (messagingService.AskYesNo(Strings.Message_KeyDeleteConfirmation))
            {
                int itemIndex = Items.IndexOf(SelectedItem);

                Items.RemoveAt(itemIndex);
                itemIndex = Math.Max(0, Math.Min(Items.Count - 1, itemIndex));
                if (Items.Any())
                    SelectedItem = Items[itemIndex];
            }
        }

        private void DoMoveItemDown()
        {
            Items.Move(SelectedItemIndex, SelectedItemIndex + 1);
        }

        private void DoMoveItemUp()
        {
            Items.Move(SelectedItemIndex, SelectedItemIndex - 1);
        }

        private void DoAddItem()
        {
            var newItem = new KeySequenceItemEditorViewModel();
            Items.Add(newItem);
            SelectedItem = newItem;
        }

        private void BuildCommands()
        {
            AddItemCommand = new AppCommand(obj => DoAddItem());

            var firstItemSelectedCondition = new LambdaCondition<KeySequenceNumpadActionEditorViewModel>(this, vm => vm.SelectedItemIndex == 0, false);
            var lastItemSelectedCondition = new LambdaCondition<KeySequenceNumpadActionEditorViewModel>(this, vm => vm.SelectedItemIndex == vm.Items.Count - 1, false);
            var anyItemSelectedCondition = new LambdaCondition<KeySequenceNumpadActionEditorViewModel>(this, vm => vm.SelectedItem != null, false);

            MoveItemUpCommand = new AppCommand(obj => DoMoveItemUp(), anyItemSelectedCondition & !firstItemSelectedCondition);
            MoveItemDownCommand = new AppCommand(obj => DoMoveItemDown(), anyItemSelectedCondition & !lastItemSelectedCondition);
            DeleteItemCommand = new AppCommand(obj => DoDeleteItem(), anyItemSelectedCondition);
        }

        public KeySequenceNumpadActionEditorViewModel(KeySequenceNumpadAction keySequenceAction, IMessagingService messagingService)
        {
            this.messagingService = messagingService;

            Items = new ObservableCollection<KeySequenceItemEditorViewModel>();

            if (keySequenceAction.Keys != null && keySequenceAction.Keys.Count > 0)
                for (int i = 0; i < keySequenceAction.Keys.Count; i++)
                {
                    var item = new KeySequenceItemEditorViewModel(keySequenceAction.Keys[i]);
                    Items.Add(item);
                }

            BuildCommands();
        }

        public KeySequenceNumpadActionEditorViewModel(IMessagingService messagingService)
        {
            this.messagingService = messagingService;

            Items = new ObservableCollection<KeySequenceItemEditorViewModel>();

            BuildCommands();
        }

        public override NumpadAction BuildModel()
        {
            var result = new KeySequenceNumpadAction();
            result.Keys = Items.Select(i => i.BuildModel()).ToList();

            return result;
        }

        public ObservableCollection<KeySequenceItemEditorViewModel> Items { get; }

        public KeySequenceItemEditorViewModel SelectedItem
        {
            get => selectedItem;
            set => Set(ref selectedItem, value);
        }

        public int SelectedItemIndex
        {
            get => selectedItemIndex;
            set => Set(ref selectedItemIndex, value);
        }

        public ICommand AddItemCommand { get; private set; }
        public ICommand MoveItemUpCommand { get; private set; }
        public ICommand MoveItemDownCommand { get; private set; }
        public ICommand DeleteItemCommand { get; private set; }
    }
}