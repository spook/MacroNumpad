﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using MacroNumpad.BusinessLogic.Services.Config;
using MacroNumpad.BusinessLogic.Services.Dialogs;
using MacroNumpad.BusinessLogic.Services.Messaging;
using MacroNumpad.BusinessLogic.ViewModels.Base;
using MacroNumpad.Resources;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MacroNumpad.BusinessLogic.ViewModels.Definition
{
    public class DefinitionEditorWindowViewModel : BaseViewModel
    {
        // Private fields -----------------------------------------------------

        private readonly IDefinitionEditorWindowAccess access;
        private readonly IConfigurationService configurationService;
        private readonly IDialogService dialogService;
        private readonly IMessagingService messagingService;
        private NumpadScreenEditorViewModel selectedScreen;
        private BaseCondition screenSelectedCondition;

        // Private methods ----------------------------------------------------

        private NumpadDefinition BuildModel()
        {
            var result = new NumpadDefinition();
            
            result.Screens = Screens.Select(s => s.BuildModel()).ToList();

            return result;
        }

        private void DoOk()
        {
            Result = BuildModel();
            access.Close(true);
        }

        private void DoCancel()
        {
            Result = null;
            access.Close(false);
        }

        private void DoAddScreen()
        {
            var newId = (!Screens.Any())? 0 : Screens.Max(s => s.Id) + 1;
            var newScreen = new NumpadScreenEditorViewModel(newId, dialogService, messagingService);
            Screens.Add(newScreen);
            SelectedScreen = newScreen;
        }

        private void DoDeleteScreen()
        {
            string message = String.Format(Strings.Message_ScreenDeleteConfirmation, SelectedScreen.Header);
            if (messagingService.AskYesNo(message))
            {
                int oldIndex = Screens.IndexOf(SelectedScreen);
                Screens.RemoveAt(oldIndex);

                oldIndex = Math.Max(0, Math.Min(oldIndex, Screens.Count - 1));
                if (Screens.Count > 0)
                    SelectedScreen = Screens[oldIndex];
            }
        }

        private void BuildScreens()
        {
            var currentDefinition = configurationService.Configuration.Definition;

            if (currentDefinition != null && currentDefinition.Screens != null)
            {
                foreach (var screen in currentDefinition?.Screens)
                {
                    var screenViewModel = new NumpadScreenEditorViewModel(screen, dialogService, messagingService);
                    Screens.Add(screenViewModel);
                }
            }
        }

        // Public methods -----------------------------------------------------

        public DefinitionEditorWindowViewModel(IDefinitionEditorWindowAccess access, 
            IConfigurationService configurationService,
            IDialogService dialogService,
            IMessagingService messagingService)
        {
            this.access = access;
            this.configurationService = configurationService;
            this.dialogService = dialogService;
            this.messagingService = messagingService;

            Screens = new ObservableCollection<NumpadScreenEditorViewModel>();

            BuildScreens();

            screenSelectedCondition = new LambdaCondition<DefinitionEditorWindowViewModel>(this, vm => vm.SelectedScreen != null, false);

            OkCommand = new AppCommand(obj => DoOk());
            CancelCommand = new AppCommand(obj => DoCancel());
            AddScreenCommand = new AppCommand(obj => DoAddScreen());
            DeleteScreenCommand = new AppCommand(obj => DoDeleteScreen(), screenSelectedCondition);
        }

        // Public properties --------------------------------------------------

        public ObservableCollection<NumpadScreenEditorViewModel> Screens { get; }

        public NumpadScreenEditorViewModel SelectedScreen
        {
            get => selectedScreen;
            set => Set(ref selectedScreen, value);
        }

        public NumpadDefinition Result { get; private set; }

        public ICommand AddScreenCommand { get; }
        public ICommand DeleteScreenCommand { get; }
        public ICommand OkCommand { get; }
        public ICommand CancelCommand { get; }
    }
}
