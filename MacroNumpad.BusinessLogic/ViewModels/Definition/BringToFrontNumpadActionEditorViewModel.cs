﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;

namespace MacroNumpad.BusinessLogic.ViewModels.Definition
{
    public class BringToFrontNumpadActionEditorViewModel : NumpadKeyActionEditorViewModel
    {
        private string path;
        private string executable;

        public BringToFrontNumpadActionEditorViewModel(BringToFrontNumpadAction bringToFrontAction)
        {
            this.path = bringToFrontAction.Executable;
        }

        public BringToFrontNumpadActionEditorViewModel()
        {
            this.path = null;
        }

        public override NumpadAction BuildModel()
        {
            var result = new BringToFrontNumpadAction();
            result.Executable = this.Executable;
            result.Path = this.Path;
            return result;
        }

        public string Path
        {
            get => path;
            set => Set(ref path, value);
        }

        public string Executable
        {
            get => executable;
            set => Set(ref executable, value);
        }
    }
}