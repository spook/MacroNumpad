﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.ViewModels.Definition
{
    public interface INumpadKeyEditorHandler
    {
        void NotifySelected(NumpadKeyEditorViewModel viewModel);
    }
}
