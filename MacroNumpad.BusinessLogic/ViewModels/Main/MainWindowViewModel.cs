﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using MacroNumpad.BusinessLogic.Services.Config;
using MacroNumpad.BusinessLogic.Services.Dialogs;
using MacroNumpad.BusinessLogic.Services.Macros;
using MacroNumpad.BusinessLogic.Services.Processor;
using MacroNumpad.BusinessLogic.Services.Serial;
using MacroNumpad.BusinessLogic.Types;
using MacroNumpad.BusinessLogic.ViewModels.Base;
using MacroNumpad.Resources;
using Spooksoft.VisualStateManager.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MacroNumpad.BusinessLogic.ViewModels.Main
{
    public class MainWindowViewModel : BaseViewModel
    {
        // Private fields -----------------------------------------------------

        private readonly IMainWindowAccess access;
        private readonly ISerialPortService serialPortService;
        private readonly ISerialProcessor processor;
        private readonly IDialogService dialogService;
        private readonly IConfigurationService configurationService;
        private readonly IMacroExecutorService macroExecutorService;
        private ScreenViewModel currentScreen;

        // Private methods ----------------------------------------------------

        private void DoHandleTrayIconClick()
        {
            access.ShowAndPositionWindow(false);
        }

        private void DoExit()
        {
            access.CloseApplication();
        }

        private void DoEdit()
        {
            (bool result, NumpadDefinition newDefinition) = dialogService.ShowDefinitionEditor();
            
            if (result)
            {
                configurationService.Configuration.Definition = newDefinition;
                configurationService.Save();

                LoadMainScreen();
            }
        }

        private void DoSwitchPortCommand(string newPort)
        {
            serialPortService.ClosePort();
            serialPortService.OpenPort(newPort);
        }

        private void HandleNumpadKeyPressed(object sender, KeyPressedEventArgs e)
        {
            access.RunOnMainThread(() =>
            {
                if (CurrentScreen == null)
                    return;

                int key = (int)(byte)e.Key - (int)(byte)NumpadKeys.NumLock;
                if (key >= 0 && key < Enum.GetValues(typeof(NumpadKeys)).Length)
                {
                    if (key < CurrentScreen.Keys.Count)
                    {
                        macroExecutorService.Execute(CurrentScreen.Keys[key].Actions);
                    }
                }
            });
        }

        private void HandleNumpadKeyHeld(object sender, EventArgs e)
        {
            // Display main window with timer
            access.RunOnMainThread(() => access.ShowAndPositionWindow(true));
        }

        private void LoadMainScreen()
        {
            var firstScreen = configurationService.Configuration.Definition?.Screens.OrderBy(s => s.Id).FirstOrDefault();
            if (firstScreen != null)
            {
                CurrentScreen = new ScreenViewModel(firstScreen);
            }
            else
            {
                CurrentScreen = null;
            }
        }

        private void HandleSwitchScreen(object sender, SwitchScreenEventArgs e)
        {
            access.RunOnMainThread(() =>
            {
                var screen = configurationService.Configuration.Definition.Screens.FirstOrDefault(s => s.Id == e.Id);
                if (screen != null)
                {
                    CurrentScreen = new ScreenViewModel(screen);
                }
            });
        }

        // Public methods -----------------------------------------------------

        public MainWindowViewModel(IMainWindowAccess access, 
            ISerialPortService serialPortService, 
            ISerialProcessor processor,
            IDialogService dialogService,
            IConfigurationService configurationService,
            IMacroExecutorService macroExecutorService)
        {
            this.access = access;
            this.serialPortService = serialPortService;
            this.processor = processor;
            this.dialogService = dialogService;
            this.configurationService = configurationService;
            this.macroExecutorService = macroExecutorService;

            processor.KeyHeld += HandleNumpadKeyHeld;
            processor.KeyPressed += HandleNumpadKeyPressed;

            macroExecutorService.SwitchScreen += HandleSwitchScreen;

            TrayMenuItems = new ObservableCollection<TrayMenuItemViewModel>();

            TrayIconClickCommand = new AppCommand(obj => DoHandleTrayIconClick());
            SwitchPortCommand = new AppCommand(obj => DoSwitchPortCommand((string)obj));
            ExitCommand = new AppCommand(obj => DoExit());
            EditCommand = new AppCommand(obj => DoEdit());

            LoadMainScreen();
        }

        public void NotifyKeyPressed(int key)
        {
            if (CurrentScreen != null && key < CurrentScreen.Keys.Count)
            {
                macroExecutorService.Execute(CurrentScreen.Keys[key].Actions);
            }
        }

        public void UpdateTrayMenuItems()
        {
            TrayMenuItems.Clear();

            var ports = serialPortService.GetAvailablePorts();

            foreach (var port in ports)
                TrayMenuItems.Add(new TrayMenuItemViewModel(port, SwitchPortCommand, port, serialPortService.Port == port));

            TrayMenuItems.Add(new TrayMenuItemViewModel(Strings.Tray_ContextMenu_Edit, EditCommand));
            TrayMenuItems.Add(new TrayMenuItemViewModel(Strings.Tray_ContextMenu_Exit, ExitCommand));            
        }

        // Public properties --------------------------------------------------

        public ICommand TrayIconClickCommand { get; }
        public ICommand ExitCommand { get; }
        public ICommand EditCommand { get; }
        public ICommand SwitchPortCommand { get; }
        public ObservableCollection<TrayMenuItemViewModel> TrayMenuItems { get; }

        public ScreenViewModel CurrentScreen
        {
            get => currentScreen;
            set => Set(ref currentScreen, value);
        }
    }
}
