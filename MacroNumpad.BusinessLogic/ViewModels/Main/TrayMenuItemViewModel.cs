﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MacroNumpad.BusinessLogic.ViewModels.Main
{
    public class TrayMenuItemViewModel
    {
        public TrayMenuItemViewModel(string header, ICommand command, object commandParameter = null, bool isChecked = false)
        {
            Header = header;
            Command = command;
            CommandParameter = commandParameter;
            IsChecked = isChecked;
        }

        public string Header { get; }
        public ICommand Command { get; }
        public object CommandParameter { get; }
        public bool IsChecked { get; }
    }
}
