﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using MacroNumpad.Common.Wpf.Helpers;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Media;

namespace MacroNumpad.BusinessLogic.ViewModels.Main
{
    public class KeyViewModel
    {
        public KeyViewModel()
        {
        }

        public KeyViewModel(NumpadKey numpadKey)
        {
            IconSource = ImageSourceHelper.FromBitmap(numpadKey.Icon);
            Title = numpadKey.Title;
            Actions = numpadKey.Actions;
        }

        public ImageSource IconSource { get; }
        public string Title { get; }
        public IReadOnlyList<NumpadAction> Actions { get; }
    }
}