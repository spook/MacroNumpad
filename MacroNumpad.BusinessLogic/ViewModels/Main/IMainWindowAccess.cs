﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.ViewModels.Main
{
    public interface IMainWindowAccess
    {
        void ShowAndPositionWindow(bool withTimer);
        void CloseApplication();
        void RunOnMainThread(Action action);
    }
}
