﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using MacroNumpad.BusinessLogic.Types;
using MacroNumpad.BusinessLogic.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.ViewModels.Main
{
    public class ScreenViewModel : BaseViewModel
    {
        public ScreenViewModel(NumpadScreen screen)
        {
            this.Id = screen.Id;
            this.Header = screen.Header;

            var keys = new List<KeyViewModel>();

            if (screen.Keys.Count > 0)
            {
                for (int i = 0; i < screen.Keys.Count; i++)
                {
                    var keyViewModel = new KeyViewModel(screen.Keys[i]);
                    keys.Add(keyViewModel);
                }

                while (keys.Count < Enum.GetValues(typeof(NumpadKeys)).Length)
                    keys.Add(new KeyViewModel());
            }

            Keys = keys;
        }

        public IReadOnlyList<KeyViewModel> Keys { get; }
        public int Id { get; }
        public string Header { get; }
    }
}
