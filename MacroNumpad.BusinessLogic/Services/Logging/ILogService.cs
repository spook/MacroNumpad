using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Services.Logging
{
    public interface ILogService
    {
        void AddLog(string source, string message);
    }
}