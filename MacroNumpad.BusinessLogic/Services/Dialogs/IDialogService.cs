﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Services.Dialogs
{
    public interface IDialogService
    {
        (bool result, string filename) ShowOpenDialog(string filter = null, string title = null, string filename = null);
        (bool result, string filename) ShowSaveDialog(string filter = null, string title = null, string filename = null);
        (bool result, NumpadDefinition newDefinition) ShowDefinitionEditor();
        void ShowExceptionDialog(Exception e);
    }
}
