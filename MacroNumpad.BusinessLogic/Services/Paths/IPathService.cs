﻿namespace MacroNumpad.BusinessLogic.Services.Paths
{
    public interface IPathService
    {
        string AppDataPath { get; }
        string ConfigFilePath { get; }
    }
}