﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Services.Paths
{
    internal class PathService : IPathService
    {
        private const string PUBLISHER = "Publisher";
        private const string APPNAME = "Application";
        private const string CONFIG_FILENAME = "Config.xml";

        private readonly string appDataPath;
        private readonly string configFilePath;

        public PathService()
        {
            appDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), PUBLISHER, APPNAME);
            configFilePath = Path.Combine(appDataPath, CONFIG_FILENAME);

            Directory.CreateDirectory(appDataPath);
        }

        public string AppDataPath => appDataPath;

        public string ConfigFilePath => configFilePath;
    }
}
