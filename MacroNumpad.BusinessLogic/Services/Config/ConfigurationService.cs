﻿using MacroNumpad.BusinessLogic.Models.Config;
using MacroNumpad.BusinessLogic.Services.Paths;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MacroNumpad.BusinessLogic.Services.Config
{
    public class ConfigurationService : IConfigurationService
    {
        // Private fields -----------------------------------------------------

        private Configuration configuration;
        private readonly IPathService pathService;

        // Private methods ----------------------------------------------------

        private string GetConfigPath() => pathService.ConfigFilePath;

        // Public methods -----------------------------------------------------

        public ConfigurationService(IPathService pathService)
        {
            this.pathService = pathService;

            // Defaults
            configuration = new Configuration();

            // Load configuration
            Load();
        }

        public bool Save()
        {
            try
            {
                string configPath = GetConfigPath();

                var configDirectory = Path.GetDirectoryName(configPath);
                if (!Directory.Exists(configDirectory))
                    Directory.CreateDirectory(configDirectory);

                XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
                using (FileStream fs = new FileStream(configPath, FileMode.Create, FileAccess.ReadWrite))
                    serializer.Serialize(fs, configuration);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Load()
        {
            try
            {
                string configPath = GetConfigPath();

                if (File.Exists(configPath))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Configuration));
                    using (FileStream fs = new FileStream(configPath, FileMode.Open, FileAccess.Read))
                    {
                        Configuration newConfiguration = serializer.Deserialize(fs) as Configuration;

                        // Possible validation

                        configuration = newConfiguration;
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        // Public properties --------------------------------------------------

        public Configuration Configuration => configuration;
    }
}
