﻿using MacroNumpad.BusinessLogic.Models.Config;

namespace MacroNumpad.BusinessLogic.Services.Config
{
    public interface IConfigurationService
    {
        Configuration Configuration { get; }

        bool Load();
        bool Save();
    }
}