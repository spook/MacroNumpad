﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Services.Serial
{
    public interface ISerialPortService
    {
        void ClosePort();
        string[] GetAvailablePorts();
        bool OpenPort(string port);
        (bool result, byte data) ReadByte();
        int ReadBytes(byte[] byteBuffer, int count);

        string Port { get; }

        event EventHandler DataReceived;
        event EventHandler Connected;
        event EventHandler Disconnected;
    }
}
