﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using MacroNumpad.BusinessLogic.Services.Messaging;
using MacroNumpad.Resources;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Services.Macros
{
    public class SwitchScreenEventArgs : EventArgs
    {
        public SwitchScreenEventArgs(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }

    public delegate void SwitchScreenEventHandler(object sender, SwitchScreenEventArgs e);

    internal class MacroExecutorService : IMacroExecutorService, IDisposable
    {
        private readonly MacroNumpadNative native;
        private readonly IMessagingService messagingService;

        private void DoSwitchScreen(int targetId)
        {
            SwitchScreen?.Invoke(this, new SwitchScreenEventArgs(targetId));
        }

        private void DoExecuteCommand(string command)
        {
            int i = 0;
            bool inQuotes = false;

            while (i < command.Length && (inQuotes || command[i] != ' '))
            {
                if (command[i] == '"')
                    inQuotes = !inQuotes;

                i++;
            }

            try
            {
                var startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.UseShellExecute = true;

                if (i < command.Length)
                {
                    string fileName = command.Substring(0, i);
                    string args = command.Substring(i + 1);

                    startInfo.FileName = fileName;
                    startInfo.Arguments = args;

                    Process.Start(startInfo);
                }
                else
                {
                    startInfo.FileName = command;

                    Process.Start(startInfo);
                }
            }
            catch (Exception e)
            {
                messagingService.ShowError(string.Format(Strings.Message_CannotStartApplication, command));
            }
        }

        private void DoBringToFront(string executable, string path)
        {
            bool result = native.ActivateMainWindow(executable);

            if (!result && !string.IsNullOrEmpty(path))
            {
                DoExecuteCommand(path);
            }
        }

        private void DoKeySequence(List<KeySequenceItem> keys)
        {
            native.SendKeys(keys);
        }

        public MacroExecutorService(IMessagingService messagingService)
        {
            native = new MacroNumpadNative();
            this.messagingService = messagingService;
        }

        public void Execute(IEnumerable<NumpadAction> actions)
        {
            foreach (var action in actions)
            {
                if (action is BringToFrontNumpadAction bringToFrontAction)
                {
                    DoBringToFront(bringToFrontAction.Executable, bringToFrontAction.Path);
                }
                else if (action is ExecuteNumpadAction executeAction)
                {
                    DoExecuteCommand(executeAction.Path);
                }
                else if (action is KeySequenceNumpadAction keySequence)
                {
                    DoKeySequence(keySequence.Keys);
                }
                else if (action is SwitchScreenNumpadAction switchScreenAction)
                {
                    DoSwitchScreen(switchScreenAction.TargetId);
                }
            }
        }

        public void Dispose()
        {
            native.Dispose();
        }

        public event SwitchScreenEventHandler SwitchScreen;
    }
}
