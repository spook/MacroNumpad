﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Services.Macros
{
    internal class MacroNumpadNative : IDisposable
    {
        // Private fields -----------------------------------------------------

        private IntPtr instance;

        // Private methods ----------------------------------------------------

        [DllImport("MacroNumpad.Native.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern void Initialize();

        [DllImport("MacroNumpad.Native.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern void Deinitialize();

        [DllImport("MacroNumpad.Native.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr CreateWindowHandler();

        [DllImport("MacroNumpad.Native.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern void DestroyWindowHandler(IntPtr handler);

        [DllImport("MacroNumpad.Native.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern bool ActivateMainWindow(IntPtr handler, [MarshalAs(UnmanagedType.LPWStr)] string processName);

        [DllImport("MacroNumpad.Native.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern bool SendKeys(IntPtr handler, byte[] operations, int length);

        // Public methods -----------------------------------------------------

        public MacroNumpadNative()
        {
            Initialize();
            instance = CreateWindowHandler();
        }

        public bool ActivateMainWindow(string processName)
        {
            if (processName == null)
                return false;

            return ActivateMainWindow(instance, processName);
        }

        public void SendKeys(IEnumerable<KeySequenceItem> keys)
        {
            List<byte> data = new List<byte>();

            foreach (var key in keys)
            {
                data.Add((byte)key.ActionType);
                data.Add((byte)key.Key);
            }

            var dataArr = data.ToArray();
            SendKeys(instance, dataArr, dataArr.Length);
        }

        public void Dispose()
        {
            DestroyWindowHandler(instance);
            instance = IntPtr.Zero;

            Deinitialize();
        }
    }
}
