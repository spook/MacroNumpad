﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Services.Macros
{
    public interface IMacroExecutorService
    {
        event SwitchScreenEventHandler SwitchScreen;

        void Execute(IEnumerable<NumpadAction> actions);
    }
}
