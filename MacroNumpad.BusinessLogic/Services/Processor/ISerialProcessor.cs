using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Services.Processor
{
    public interface ISerialProcessor
    {
        event EventHandler KeyHeld;
        event KeyPressedEventHandler KeyPressed;
    }
}