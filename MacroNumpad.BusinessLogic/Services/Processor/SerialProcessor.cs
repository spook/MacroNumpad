using MacroNumpad.BusinessLogic.Services.Logging;
using MacroNumpad.BusinessLogic.Services.Serial;
using MacroNumpad.BusinessLogic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Services.Processor
{
    public class KeyPressedEventArgs : EventArgs
    {
        public KeyPressedEventArgs(NumpadKeys key)
        {
            Key = key;
        }

        public NumpadKeys Key { get; }
    }

    public delegate void KeyPressedEventHandler(object sender, KeyPressedEventArgs e);

    internal class SerialProcessor : ISerialProcessor
    {
        // Private constants --------------------------------------------------

        private readonly TimeSpan holdTimeSpan = TimeSpan.FromMilliseconds(300);
        private const string SERIAL_PROCESSOR_LOG_SOURCE = "Serial Processor";
        private const int OPERATION_KEY_DOWN = 1;
        private const int OPERATION_KEY_UP = 2;

        // Private types ------------------------------------------------------

        private enum SerialProcessorMode
        {
            ReadingOperation,
            ReadingKey
        };

        // Private fields -----------------------------------------------------

        private readonly ISerialPortService serialPortService;
        private readonly ILogService logService;

        private readonly Dictionary<byte, long> keypresses = new Dictionary<byte, long>();

        private SerialProcessorMode mode;
        private byte operation;
        private byte key;

        // Private methods ----------------------------------------------------

        private void ProcessReceivedData(byte operation, byte key)
        {
            switch (operation)
            {
                case OPERATION_KEY_DOWN:
                    {
                        // Key down
                        keypresses[key] = DateTime.Now.Ticks;
                        break;
                    }
                case OPERATION_KEY_UP:
                    {
                        // Key up
                        long keydown;
                        long keyup;
                        if (keypresses.ContainsKey(key))
                        {
                            keydown = keypresses[key];
                            keyup = DateTime.Now.Ticks;
                            keypresses.Remove(key);
                        }
                        else
                        {
                            keydown = 0;
                            keyup = 0;
                        }

                        long tickDiff = keyup - keydown;
                        if (TimeSpan.FromTicks(tickDiff) > holdTimeSpan)
                        {
                            KeyHeld?.Invoke(this, EventArgs.Empty);
                        }
                        else
                        {
                            if (Enum.IsDefined(typeof(NumpadKeys), key))
                            {
                                KeyPressed?.Invoke(this, new KeyPressedEventArgs((NumpadKeys)key));
                            }
                            else
                            {
                                #if DEBUG
                                logService.AddLog(SERIAL_PROCESSOR_LOG_SOURCE, $"Unknown key press: {key}");
                                #endif

                            }
                        }

                        break;
                    }
                default:
                    {
                        #if DEBUG
                        logService.AddLog(SERIAL_PROCESSOR_LOG_SOURCE, $"Unknown operation: {operation} (key: {key})");
                        #endif

                        break;
                    }
            }
        }

        private void HandleSerialPortDataReceived(object sender, EventArgs e)
        {
            bool canRead = true;

            while (canRead)
            {
                (bool result, byte data) = serialPortService.ReadByte();
                if (!result)
                    return;

                switch (mode)
                {
                    case SerialProcessorMode.ReadingOperation:
                        {
                            operation = data;
                            mode = SerialProcessorMode.ReadingKey;
                            break;
                        }
                    case SerialProcessorMode.ReadingKey:
                        {
                            key = data;
                            ProcessReceivedData(operation, key);

                            operation = 0;
                            key = 0;
                            mode = SerialProcessorMode.ReadingOperation;
                            break;
                        }
                }
            }
        }

        private void HandleSerialPortDisconnected(object sender, EventArgs e)
        {
            // Reset
            key = 0;
            operation = 0;
            mode = SerialProcessorMode.ReadingOperation;
        }

        // Public methods -----------------------------------------------------

        public SerialProcessor(ISerialPortService serialPortService, ILogService logService)
        {
            this.serialPortService = serialPortService;
            this.logService = logService;

            serialPortService.Disconnected += HandleSerialPortDisconnected;
            serialPortService.DataReceived += HandleSerialPortDataReceived;
        }

        // Public properties --------------------------------------------------

        public event EventHandler KeyHeld;
        public event KeyPressedEventHandler KeyPressed;
    }
}