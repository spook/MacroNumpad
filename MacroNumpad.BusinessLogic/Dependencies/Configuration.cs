﻿using MacroNumpad.BusinessLogic.Services.Config;
using MacroNumpad.BusinessLogic.Services.EventBus;
using MacroNumpad.BusinessLogic.Services.Logging;
using MacroNumpad.BusinessLogic.Services.Macros;
using MacroNumpad.BusinessLogic.Services.Paths;
using MacroNumpad.BusinessLogic.Services.Processor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Unity.Lifetime;

namespace MacroNumpad.BusinessLogic.Dependencies
{
    public static class Configuration
    {
        private static bool isConfigured = false;

        public static void Configure(IUnityContainer container)
        {
            if (isConfigured)
                return;
            isConfigured = true;

            // Register types
            container.RegisterType<IEventBus, EventBus>(new ContainerControlledLifetimeManager());
            container.RegisterType<IPathService, PathService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IConfigurationService, ConfigurationService>(new ContainerControlledLifetimeManager());
            container.RegisterType<ILogService, LogService>(new ContainerControlledLifetimeManager());
            container.RegisterType<ISerialProcessor, SerialProcessor>(new ContainerControlledLifetimeManager());
            container.RegisterType<IMacroExecutorService, MacroExecutorService>(new ContainerControlledLifetimeManager());
        }
    }
}
