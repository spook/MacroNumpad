﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Types
{
    public enum NumpadKeys : byte
    {        
        NumLock = 128,
        D0,
        D1,
        D2,
        D3,
        D4,
        D5,
        D6,
        D7,
        D8,
        D9,
        Dot,
        Plus,
        Minus,
        Slash,
        Asterisk,
        Enter
    }
}
