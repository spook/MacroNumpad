﻿using MacroNumpad.Common.Types;
using MacroNumpad.Common.Wpf.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Types
{
    [TypeConverter(typeof(EnumResourceConverter))]
    public enum KeyActionType : byte
    {
        [EnumResource("ActionType_Press")] Press = 0,
        [EnumResource("ActionType_PressAndRelease")] PressAndRelease = 1,
        [EnumResource("ActionType_Release")] Release = 2
    }
}
