﻿using MacroNumpad.Common.Types;
using MacroNumpad.Common.Wpf.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.BusinessLogic.Types
{
    [TypeConverter(typeof(EnumResourceConverter))]
    public enum Keys
    {
        [EnumResource("Key_MouseLeft")] MouseLeft = 0x01, // Left mouse button
        [EnumResource("Key_MouseRight")] MouseRight = 0x02, // Right mouse button
        [EnumResource("Key_CtrlBreak")] CtrlBreak = 0x03, // Control-break processing
        [EnumResource("Key_MouseMiddle")] MouseMiddle = 0x04, // Middle mouse button (three-button mouse)
        [EnumResource("Key_MouseX1")] MouseX1 = 0x05, // X1 mouse button
        [EnumResource("Key_MouseX2")] MouseX2 = 0x06, // X2 mouse button
        [EnumResource("Key_Backspace")] Backspace = 0x08, // BACKSPACE key
        [EnumResource("Key_Tab")] Tab = 0x09, // TAB key
        [EnumResource("Key_Clear")] Clear = 0x0C, // CLEAR key
        [EnumResource("Key_Enter")] Enter = 0x0D, // ENTER key
        [EnumResource("Key_Shift")] Shift = 0x10, // SHIFT key
        [EnumResource("Key_Ctrl")] Ctrl = 0x11, // CTRL key
        [EnumResource("Key_Alt")] Alt = 0x12, // ALT key
        [EnumResource("Key_Pause")] Pause = 0x13, // PAUSE key
        [EnumResource("Key_CapsLock")] CapsLock = 0x14, // CAPS LOCK key
        [EnumResource("Key_Escape")] Escape = 0x1B, // ESC key
        [EnumResource("Key_Space")] Space = 0x20, // SPACEBAR
        [EnumResource("Key_PageUp")] PageUp = 0x21, // PAGE UP key
        [EnumResource("Key_PageDown")] PageDown = 0x22, // PAGE DOWN key
        [EnumResource("Key_End")] End = 0x23, // END key
        [EnumResource("Key_Home")] Home = 0x24, // HOME key
        [EnumResource("Key_Left")] Left = 0x25, // LEFT ARROW key
        [EnumResource("Key_Up")] Up = 0x26, // UP ARROW key
        [EnumResource("Key_Right")] Right = 0x27, // RIGHT ARROW key
        [EnumResource("Key_Down")] Down = 0x28, // DOWN ARROW key
        [EnumResource("Key_Select")] Select = 0x29, // SELECT key
        [EnumResource("Key_Print")] Print = 0x2A, // PRINT key
        [EnumResource("Key_Execute")] Execute = 0x2B, // EXECUTE key
        [EnumResource("Key_PrintScreen")] PrintScreen = 0x2C, // PRINT SCREEN key
        [EnumResource("Key_Insert")] Insert = 0x2D, // INS key
        [EnumResource("Key_Delete")] Delete = 0x2E, // DEL key
        [EnumResource("Key_Help")] Help = 0x2F, // HELP key
        [EnumResource("Key_D0")] D0 = 0x30, // 0 key
        [EnumResource("Key_D1")] D1 = 0x31, // 1 key
        [EnumResource("Key_D2")] D2 = 0x32, // 2 key
        [EnumResource("Key_D3")] D3 = 0x33, // 3 key
        [EnumResource("Key_D4")] D4 = 0x34, // 4 key
        [EnumResource("Key_D5")] D5 = 0x35, // 5 key
        [EnumResource("Key_D6")] D6 = 0x36, // 6 key
        [EnumResource("Key_D7")] D7 = 0x37, // 7 key
        [EnumResource("Key_D8")] D8 = 0x38, // 8 key
        [EnumResource("Key_D9")] D9 = 0x39, // 9 key
        [EnumResource("Key_A")] A = 0x41, // A key
        [EnumResource("Key_B")] B = 0x42, // B key
        [EnumResource("Key_C")] C = 0x43, // C key
        [EnumResource("Key_D")] D = 0x44, // D key
        [EnumResource("Key_E")] E = 0x45, // E key
        [EnumResource("Key_F")] F = 0x46, // F key
        [EnumResource("Key_G")] G = 0x47, // G key
        [EnumResource("Key_H")] H = 0x48, // H key
        [EnumResource("Key_I")] I = 0x49, // I key
        [EnumResource("Key_J")] J = 0x4A, // J key
        [EnumResource("Key_K")] K = 0x4B, // K key
        [EnumResource("Key_L")] L = 0x4C, // L key
        [EnumResource("Key_M")] M = 0x4D, // M key
        [EnumResource("Key_N")] N = 0x4E, // N key
        [EnumResource("Key_O")] O = 0x4F, // O key
        [EnumResource("Key_P")] P = 0x50, // P key
        [EnumResource("Key_Q")] Q = 0x51, // Q key
        [EnumResource("Key_R")] R = 0x52, // R key
        [EnumResource("Key_S")] S = 0x53, // S key
        [EnumResource("Key_T")] T = 0x54, // T key
        [EnumResource("Key_U")] U = 0x55, // U key
        [EnumResource("Key_V")] V = 0x56, // V key
        [EnumResource("Key_W")] W = 0x57, // W key
        [EnumResource("Key_X")] X = 0x58, // X key
        [EnumResource("Key_Y")] Y = 0x59, // Y key
        [EnumResource("Key_Z")] Z = 0x5A, // Z key
        [EnumResource("Key_LeftWin")] LeftWin = 0x5B, // Left Windows key (Natural keyboard)
        [EnumResource("Key_RightWin")] RightWin = 0x5C, // Right Windows key (Natural keyboard)
        [EnumResource("Key_Applications")] Applications = 0x5D, // Applications key (Natural keyboard)
        [EnumResource("Key_Sleep")] Sleep = 0x5F, // Computer Sleep key
        [EnumResource("Key_Numpad0")] Numpad0 = 0x60, // Numeric keypad 0 key
        [EnumResource("Key_Numpad1")] Numpad1 = 0x61, // Numeric keypad 1 key
        [EnumResource("Key_Numpad2")] Numpad2 = 0x62, // Numeric keypad 2 key
        [EnumResource("Key_Numpad3")] Numpad3 = 0x63, // Numeric keypad 3 key
        [EnumResource("Key_Numpad4")] Numpad4 = 0x64, // Numeric keypad 4 key
        [EnumResource("Key_Numpad5")] Numpad5 = 0x65, // Numeric keypad 5 key
        [EnumResource("Key_Numpad6")] Numpad6 = 0x66, // Numeric keypad 6 key
        [EnumResource("Key_Numpad7")] Numpad7 = 0x67, // Numeric keypad 7 key
        [EnumResource("Key_Numpad8")] Numpad8 = 0x68, // Numeric keypad 8 key
        [EnumResource("Key_Numpad9")] Numpad9 = 0x69, // Numeric keypad 9 key
        [EnumResource("Key_Multiply")] Multiply = 0x6A, // Multiply key
        [EnumResource("Key_Add")] Add = 0x6B, // Add key
        [EnumResource("Key_Separator")] Separator = 0x6C, // Separator key
        [EnumResource("Key_Subtract")] Subtract = 0x6D, // Subtract key
        [EnumResource("Key_Decimal")] Decimal = 0x6E, // Decimal key
        [EnumResource("Key_Divide")] Divide = 0x6F, // Divide key
        [EnumResource("Key_F1")] F1 = 0x70, // F1 key
        [EnumResource("Key_F2")] F2 = 0x71, // F2 key
        [EnumResource("Key_F3")] F3 = 0x72, // F3 key
        [EnumResource("Key_F4")] F4 = 0x73, // F4 key
        [EnumResource("Key_F5")] F5 = 0x74, // F5 key
        [EnumResource("Key_F6")] F6 = 0x75, // F6 key
        [EnumResource("Key_F7")] F7 = 0x76, // F7 key
        [EnumResource("Key_F8")] F8 = 0x77, // F8 key
        [EnumResource("Key_F9")] F9 = 0x78, // F9 key
        [EnumResource("Key_F10")] F10 = 0x79, // F10 key
        [EnumResource("Key_F11")] F11 = 0x7A, // F11 key
        [EnumResource("Key_F12")] F12 = 0x7B, // F12 key
        [EnumResource("Key_F13")] F13 = 0x7C, // F13 key
        [EnumResource("Key_F14")] F14 = 0x7D, // F14 key
        [EnumResource("Key_F15")] F15 = 0x7E, // F15 key
        [EnumResource("Key_F16")] F16 = 0x7F, // F16 key
        [EnumResource("Key_F17")] F17 = 0x80, // F17 key
        [EnumResource("Key_F18")] F18 = 0x81, // F18 key
        [EnumResource("Key_F19")] F19 = 0x82, // F19 key
        [EnumResource("Key_F20")] F20 = 0x83, // F20 key
        [EnumResource("Key_F21")] F21 = 0x84, // F21 key
        [EnumResource("Key_F22")] F22 = 0x85, // F22 key
        [EnumResource("Key_F23")] F23 = 0x86, // F23 key
        [EnumResource("Key_F24")] F24 = 0x87, // F24 key
        [EnumResource("Key_NumLock")] NumLock = 0x90, // NUM LOCK key
        [EnumResource("Key_ScrollLock")] ScrollLock = 0x91, // SCROLL LOCK key
        [EnumResource("Key_LeftShift")] LeftShift = 0xA0, // Left SHIFT key
        [EnumResource("Key_RightShift")] RightShift = 0xA1, // Right SHIFT key
        [EnumResource("Key_LeftCtrl")] LeftCtrl = 0xA2, // Left CONTROL key
        [EnumResource("Key_RightCtrl")] RightCtrl = 0xA3, // Right CONTROL key
        [EnumResource("Key_LeftMenu")] LeftMenu = 0xA4, // Left MENU key
        [EnumResource("Key_RightMenu")] RightMenu = 0xA5, // Right MENU key
        [EnumResource("Key_BrowserBack")] BrowserBack = 0xA6, // Browser Back key
        [EnumResource("Key_BrowserForward")] BrowserForward = 0xA7, // Browser Forward key
        [EnumResource("Key_BrowserRefresh")] BrowserRefresh = 0xA8, // Browser Refresh key
        [EnumResource("Key_BrowserStop")] BrowserStop = 0xA9, // Browser Stop key
        [EnumResource("Key_BrowserSearch")] BrowserSearch = 0xAA, // Browser Search key
        [EnumResource("Key_BrowserFavorites")] BrowserFavorites = 0xAB, // Browser Favorites key
        [EnumResource("Key_BrowserHome")] BrowserHome = 0xAC, // Browser Start and Home key
        [EnumResource("Key_VolumeMute")] VolumeMute = 0xAD, // Volume Mute key
        [EnumResource("Key_VolumeDown")] VolumeDown = 0xAE, // Volume Down key
        [EnumResource("Key_VolumeUp")] VolumeUp = 0xAF, // Volume Up key
        [EnumResource("Key_MediaNext")] MediaNext = 0xB0, // Next Track key
        [EnumResource("Key_MediaPrevious")] MediaPrevious = 0xB1, // Previous Track key
        [EnumResource("Key_MediaStop")] MediaStop = 0xB2, // Stop Media key
        [EnumResource("Key_MediaPlayPause")] MediaPlayPause = 0xB3, // Play/Pause Media key
        [EnumResource("Key_LaunchMail")] LaunchMail = 0xB4, // Start Mail key
        [EnumResource("Key_SelectMedia")] SelectMedia = 0xB5, // Select Media key
        [EnumResource("Key_LaunchApp1")] LaunchApp1 = 0xB6, // Start Application 1 key
        [EnumResource("Key_LaunchApp2")] LaunchApp2 = 0xB7, // Start Application 2 key
    }
}
