#include <hidboot.h>
#include <usbhub.h>
#include <SPI.h>

const int MACRO_KEY_DOWN = 1;
const int MACRO_KEY_UP = 2;

class KbdRptParser : public KeyboardReportParser
{
private:
    uint8_t UsbKeyToCode(uint8_t key);

protected:
    void OnKeyDown	(uint8_t mod, uint8_t key);
    void OnKeyUp	(uint8_t mod, uint8_t key);
};

/*
NumLock = 128,
D0 = 129,
D1 = 130,
D2 = 131,
D3 = 132,
D4 = 133,
D5 = 134,
D6 = 135,
D7 = 136,
D8 = 137,
D9 = 138,
Dot = 139,
Plus = 140,
Minus = 141,
Slash = 142,
Asterisk = 143,
Enter = 144
*/

uint8_t KbdRptParser::UsbKeyToCode(uint8_t key)
{
  switch (key)
  {
    case 83 : return 128; // NumLock
    case 84 : return 142; // /
    case 85 : return 143; // *
    case 42 : return 141; // BS -> -
    case 95 : return 136; // 7
    case 96 : return 137; // 8
    case 97 : return 138; // 9
    case 86 : return 140; // - -> +
    case 92 : return 133; // 4
    case 93 : return 134; // 5
    case 94 : return 135; // 6
    case 87 : return 140; // + -> +
    case 89 : return 130; // 1
    case 90 : return 131; // 2
    case 91 : return 132; // 3
    case 88 : return 144; // Enter
    case 98 : return 129; // 0
    case 99 : return 139; // .
    default: return 0;
  }
}

void KbdRptParser::OnKeyDown(uint8_t mod, uint8_t key)
{
  Serial.write(MACRO_KEY_DOWN); 
  Serial.write(UsbKeyToCode(key));
}

void KbdRptParser::OnKeyUp(uint8_t mod, uint8_t key)
{
  Serial.write(MACRO_KEY_UP);
  Serial.write(UsbKeyToCode(key));
}

USB     Usb;
HIDBoot<USB_HID_PROTOCOL_KEYBOARD>    HidKeyboard(&Usb);

KbdRptParser Prs;

void setup()
{
  Serial.begin( 115200 );
#if !defined(__MIPSEL__)
  while (!Serial); // Wait for serial port to connect - used on Leonardo, Teensy and other boards with built-in USB CDC serial connection
#endif

  if (Usb.Init() == -1)
  {
    pinMode(13, OUTPUT);
    while (true)
    {
      digitalWrite(13, HIGH);
      delay(1000);
      digitalWrite(13, LOW);
      delay(1000);
    }
  }

  delay( 200 );

  HidKeyboard.SetReportParser(0, &Prs);
}

void loop()
{
  Usb.Task();
}
