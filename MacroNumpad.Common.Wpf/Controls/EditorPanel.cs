﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MacroNumpad.Common.Wpf.Controls
{
    public class EditorPanel : Panel
    {
        private Size DesiredSizeWithMargin(UIElement element)
        {
            if (element == null)
                return Size.Empty;

            if (element is FrameworkElement frameworkElement)
                return new Size(frameworkElement.DesiredSize.Width + frameworkElement.Margin.Left + frameworkElement.Margin.Right,
                  frameworkElement.DesiredSize.Height + frameworkElement.Margin.Top + frameworkElement.Margin.Bottom);
            else
                return element.DesiredSize;
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            double totalLabelWidth = 0.0;
            double totalEditorWidth = 0.0;
            double totalItemHeight = 0.0;

            for (int i = 0; i < InternalChildren.Count; i += 2)
            {
                // Measure label
                InternalChildren[i].Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                Size labelDesiredSize = DesiredSizeWithMargin(InternalChildren[i]);

                Size editorDesiredSize = Size.Empty;
                if (i + 1 < InternalChildren.Count)
                {
                    InternalChildren[i + 1].Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                    editorDesiredSize = DesiredSizeWithMargin(InternalChildren[i + 1]);
                }

                totalLabelWidth = Math.Max(totalLabelWidth, labelDesiredSize.Width);
                totalEditorWidth = Math.Max(totalEditorWidth, editorDesiredSize.Width);
                totalItemHeight += Math.Max(labelDesiredSize.Height, editorDesiredSize.Height);
            }

            return new Size(totalLabelWidth + totalEditorWidth, totalItemHeight);
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            // Label area width

            double labelAreaWidth = 0;
            for (int i = 0; i < InternalChildren.Count; i += 2)
                labelAreaWidth = Math.Max(labelAreaWidth, DesiredSizeWithMargin(InternalChildren[i]).Width);

            labelAreaWidth = Math.Min(labelAreaWidth, finalSize.Width);

            // Editor area width

            double editorAreaWidth = Math.Max(0, finalSize.Width - labelAreaWidth);

            // Arranging controls

            double y = 0;
            int controlIndex = 0;

            while (controlIndex < InternalChildren.Count)
            {
                // Retrieve label and editor

                UIElement label = InternalChildren[controlIndex++];
                double totalLabelHeight = DesiredSizeWithMargin(label).Height;

                UIElement editor = (controlIndex < InternalChildren.Count) ? InternalChildren[controlIndex++] : null;
                double totalEditorHeight = DesiredSizeWithMargin(editor).Height;

                double rowHeight = Math.Max(totalLabelHeight, totalEditorHeight);

                // Arrange label

                Thickness labelMargin = new Thickness();
                VerticalAlignment labelVerticalAlignment = VerticalAlignment.Top;

                if (label is FrameworkElement labelFrameworkElement)
                {
                    labelMargin = labelFrameworkElement.Margin;
                    labelVerticalAlignment = labelFrameworkElement.VerticalAlignment;
                }

                switch (labelVerticalAlignment)
                {
                    case VerticalAlignment.Top:
                        {
                            label.Arrange(new Rect(labelMargin.Left, y + labelMargin.Top, label.DesiredSize.Width, label.DesiredSize.Height));
                            break;
                        }

                    case VerticalAlignment.Center:
                        {
                            double labelY = y + (rowHeight - totalLabelHeight) / 2.0;
                            label.Arrange(new Rect(labelMargin.Left, labelY + labelMargin.Top, label.DesiredSize.Width, label.DesiredSize.Height));
                            break;
                        }

                    case VerticalAlignment.Bottom:
                        {
                            double labelY = y + rowHeight - totalLabelHeight;
                            label.Arrange(new Rect(labelMargin.Left, labelY + labelMargin.Top, label.DesiredSize.Width, label.DesiredSize.Height));
                            break;
                        }

                    case VerticalAlignment.Stretch:
                        {
                            label.Arrange(new Rect(labelMargin.Left, y + labelMargin.Top, label.DesiredSize.Width, rowHeight - (labelMargin.Top + labelMargin.Bottom)));
                            break;
                        }

                    default:
                        throw new InvalidEnumArgumentException("Unsupported vertical alignment!");
                }

                // Arrange editor

                if (editor != null)
                {
                    Thickness editorMargin = new Thickness();
                    HorizontalAlignment editorHorizontalAlignment = HorizontalAlignment.Stretch;
                    VerticalAlignment editorVerticalAlignment = VerticalAlignment.Top;

                    if (editor is FrameworkElement frameworkEditor)
                    {
                        editorMargin = frameworkEditor.Margin;
                        editorHorizontalAlignment = frameworkEditor.HorizontalAlignment;
                        editorVerticalAlignment = frameworkEditor.VerticalAlignment;
                    }

                    double editorTop;
                    double editorHeight;

                    switch (editorVerticalAlignment)
                    {
                        case VerticalAlignment.Top:
                            editorTop = y + editorMargin.Top;
                            editorHeight = editor.DesiredSize.Height;
                            break;

                        case VerticalAlignment.Center:
                            editorTop = y + (rowHeight - totalEditorHeight) / 2 + editorMargin.Top;
                            editorHeight = editor.DesiredSize.Height;
                            break;

                        case VerticalAlignment.Bottom:
                            editorTop = y + rowHeight - totalEditorHeight + editorMargin.Top;
                            editorHeight = editor.DesiredSize.Height;
                            break;

                        case VerticalAlignment.Stretch:
                            editorTop = y + editorMargin.Top;
                            editorHeight = rowHeight - (editorMargin.Top + editorMargin.Bottom);
                            break;

                        default:
                            throw new InvalidEnumArgumentException("Unsupported vertical alignment!");
                    }

                    double editorWidth;
                    double editorLeft;

                    switch (editorHorizontalAlignment)
                    {
                        case HorizontalAlignment.Left:
                            editorLeft = labelAreaWidth + editorMargin.Left;
                            editorWidth = Math.Max(0, Math.Min(editorAreaWidth - (editorMargin.Left + editorMargin.Right), editor.DesiredSize.Width));
                            break;

                        case HorizontalAlignment.Center:
                            editorLeft = labelAreaWidth + (editorAreaWidth - (editor.DesiredSize.Width + editorMargin.Left + editorMargin.Right)) / 2;
                            editorWidth = Math.Max(0, Math.Min(editorAreaWidth - (editorMargin.Left + editorMargin.Right), editor.DesiredSize.Width));
                            editor.Arrange(new Rect(editorLeft, editorTop, editorWidth, editorHeight));
                            break;

                        case HorizontalAlignment.Right:
                            editorLeft = labelAreaWidth + editorAreaWidth - (editor.DesiredSize.Width + editorMargin.Left + editorMargin.Right);
                            editorWidth = Math.Max(0, Math.Min(editorAreaWidth - (editorMargin.Left + editorMargin.Right), editor.DesiredSize.Width));
                            break;

                        case HorizontalAlignment.Stretch:
                            editorLeft = labelAreaWidth + editorMargin.Left;
                            editorWidth = editorAreaWidth - (editorMargin.Left + editorMargin.Right);
                            break;

                        default:
                            throw new InvalidOperationException("Unsupported HorizontalAlignment!");
                    }

                    editor.Arrange(new Rect(editorLeft, editorTop, editorWidth, editorHeight));
                }

                y += Math.Max(DesiredSizeWithMargin(label).Height, DesiredSizeWithMargin(editor).Height);
            }

            return finalSize;
        }
    }
}
