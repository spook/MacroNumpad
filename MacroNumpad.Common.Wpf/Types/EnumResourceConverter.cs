﻿using MacroNumpad.Common.Extensions;
using MacroNumpad.Common.Types;
using MacroNumpad.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.Common.Wpf.Types
{
    public class EnumResourceConverter : EnumConverter
    {
        private ResourceManager resourceManager;

        public EnumResourceConverter(Type type) 
            : base(type)
        {
            resourceManager = new ResourceManager(typeof(Strings));
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                if (value != null)
                    return value.GetLocalizedName();
                else
                    return string.Empty;
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
