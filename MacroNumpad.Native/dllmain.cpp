﻿// dllmain.cpp : Definiuje punkt wejścia dla aplikacji DLL.
#include "pch.h"
#include "window_handler.h"

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

__declspec(dllexport) void __stdcall Initialize()
{
    CoInitialize(nullptr);
}

__declspec(dllexport) void __stdcall Deinitialize()
{
    CoUninitialize();
}

__declspec(dllexport) void * __stdcall CreateWindowHandler()
{
    return new WindowHandler();
}

__declspec(dllexport) void __stdcall DestroyWindowHandler(void * windowHandler)
{
    auto handler = (WindowHandler*)windowHandler;
    delete handler;
}

__declspec(dllexport) bool __stdcall ActivateMainWindow(void * windowHandler, const wchar_t* processName)
{
    auto handler = (WindowHandler*)windowHandler;

    std::wstring strProcessName(processName);
    return handler->activate_main_window(strProcessName);
}

__declspec(dllexport) void __stdcall SendKeys(void* windowHandler, unsigned char* operations, int length)
{
    auto handler = (WindowHandler*)windowHandler;

    handler->sendKeys(operations, length);
}
