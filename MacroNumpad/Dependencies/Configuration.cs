﻿using MacroNumpad.BusinessLogic.Services.Dialogs;
using MacroNumpad.BusinessLogic.Services.Messaging;
using MacroNumpad.BusinessLogic.Services.Serial;
using MacroNumpad.Services.Dialogs;
using MacroNumpad.Services.Messaging;
using MacroNumpad.Services.Serial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Unity.Lifetime;

namespace MacroNumpad.Dependencies
{
    public static class Configuration
    {
        private static bool configured = false;

        public static void Configure(IUnityContainer container)
        {
            if (configured)
                return;

            container.RegisterType<IMessagingService, MessagingService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IDialogService, DialogService>(new ContainerControlledLifetimeManager());
            container.RegisterType<ISerialPortService, SerialPortService>(new ContainerControlledLifetimeManager());

            MacroNumpad.BusinessLogic.Dependencies.Configuration.Configure(container);

            configured = true;
        }
    }
}
