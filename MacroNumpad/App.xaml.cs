﻿using MacroNumpad.BusinessLogic.Services.Dialogs;
using MacroNumpad.Dependencies;
using MacroNumpad.Windows;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Unity;

namespace MacroNumpad
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private MainWindow mainWindow;
        private readonly IDialogService dialogService;

        public App()
        {
            Configuration.Configure(Container.Instance);

            mainWindow = new MainWindow();

            dialogService = Dependencies.Container.Instance.Resolve<IDialogService>();
            this.DispatcherUnhandledException += HandleException;
        }

        private void HandleException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            dialogService.ShowExceptionDialog(e.Exception);
            e.Handled = true;
        }
    }
}
