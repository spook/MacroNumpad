﻿using MacroNumpad.BusinessLogic.Services.Config;
using MacroNumpad.BusinessLogic.Services.Logging;
using MacroNumpad.BusinessLogic.Services.Serial;
using RJCP.IO.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MacroNumpad.Services.Serial
{
    internal class SerialPortService : ISerialPortService
    {
        // Private constants --------------------------------------------------

        private const string SerialPortLogSource = "Serial port service";

        // Private fields -----------------------------------------------------

        private SerialPortStream serialPortStream;
        private string port;
        private readonly IConfigurationService configurationService;
        private readonly ILogService logService;

        // Private methods ----------------------------------------------------

        private void HandleSerialPortDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            #if DEBUG
            logService.AddLog(SerialPortLogSource, $"Data received");
            #endif

            DataReceived?.Invoke(this, EventArgs.Empty);
        }

        private void HandleSerialPortErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            #if DEBUG
            logService.AddLog(SerialPortLogSource, $"Serial port received error!");
            #endif
        }

        // Public methods -----------------------------------------------------

        public SerialPortService(IConfigurationService configurationService, ILogService logService)
        {
            this.configurationService = configurationService;
            this.logService = logService;

            string[] ports = SerialPortStream.GetPortNames();
            serialPortStream = null;

            if (ports.Any(p => p == configurationService.Configuration.LastUsedPort))
                OpenPort(configurationService.Configuration.LastUsedPort);
        }

        public bool OpenPort(string port)
        {
            #if DEBUG
            logService.AddLog(SerialPortLogSource, $"Trying to open port {port}...");
#endif

            if (serialPortStream != null)
                ClosePort();

            try
            {
                serialPortStream = new SerialPortStream();
                serialPortStream.BaudRate = 115200;
                serialPortStream.DataReceived += HandleSerialPortDataReceived;
                serialPortStream.ErrorReceived += HandleSerialPortErrorReceived;
                serialPortStream.ReceivedBytesThreshold = 1;
                serialPortStream.ReadTimeout = 250;
                serialPortStream.PortName = port;

                #if DEBUG
                logService.AddLog(SerialPortLogSource, $"Opening port {port}...");
                #endif

                serialPortStream.Open();

                if (serialPortStream.IsOpen)
                {
                    this.port = port;

                    configurationService.Configuration.LastUsedPort = port;
                    configurationService.Save();

                    Connected?.Invoke(this, EventArgs.Empty);
                    DataReceived?.Invoke(this, EventArgs.Empty);
                    
                    #if DEBUG
                    logService.AddLog(SerialPortLogSource, $"Opened port {port}...");
                    #endif

                    return true;
                }
                else
                {
                    serialPortStream?.Dispose();
                    serialPortStream = null;
                    this.port = null;

                    #if DEBUG
                    logService.AddLog(SerialPortLogSource, $"Open call exited, but port is not opened");
                    #endif

                    return false;
                }                                    
            }
            catch
            {
                #if DEBUG
                logService.AddLog(SerialPortLogSource, $"Failed to open port {port}...");
                #endif

                serialPortStream?.Dispose();
                serialPortStream = null;
                this.port = null;

                return false;
            }
        }

        public void ClosePort()
        {
#if DEBUG
            logService.AddLog(SerialPortLogSource, $"Closing port {this.port}...");
#endif

            if (serialPortStream != null)
            {
                if (serialPortStream.IsOpen)
                    serialPortStream.Close();

                this.port = null;
                serialPortStream.Dispose();
            }

            serialPortStream = null;

            Disconnected?.Invoke(this, EventArgs.Empty);
        }

        public string[] GetAvailablePorts() => SerialPortStream.GetPortNames().Distinct().ToArray();

        public int ReadBytes(byte[] byteBuffer, int count)
        {
            #if DEBUG
            logService.AddLog(SerialPortLogSource, $"Attempting to read {count} bytes...");
            #endif

            if (serialPortStream == null)
                throw new InvalidOperationException("Serial port is not opened!");
                
            if (!serialPortStream.IsOpen)
            {
                #if DEBUG
                logService.AddLog(SerialPortLogSource, $"Port is not open, nothing got read.");
                #endif

                return -1;
            }

            try
            {
                int result = serialPortStream.Read(byteBuffer, 0, count);
                
                #if DEBUG
                logService.AddLog(SerialPortLogSource, $"Managed to read {result} bytes.");
                #endif

                return result;
            }
            catch (Exception e)
            {
                #if DEBUG
                logService.AddLog(SerialPortLogSource, $"Failed to read bytes: {e.Message}");
                #endif

                ClosePort();
                return -1;
            }
        }

        public (bool result, byte data) ReadByte()
        {
            if (serialPortStream == null)
                throw new InvalidOperationException("Serial port is not opened!");

            if (!serialPortStream.IsOpen)
            {
                #if DEBUG
                logService.AddLog(SerialPortLogSource, $"Port is not open, nothing got read.");
                #endif

                return (false, 0);
            }

            try
            {
                int readData = serialPortStream.ReadByte();

                if (readData < 0)
                {
                    return (false, 0);
                }
                else
                {
                    return (true, (byte)readData);
                }
            }
            catch (TimeoutException)
            {
                return (false, 0);
            }
            catch
            {
                ClosePort();
                return (false, 0);
            }
        }

        // Public properties --------------------------------------------------

        public string Port => port;

        public event EventHandler DataReceived;
        public event EventHandler Connected;
        public event EventHandler Disconnected;
    }
}
