﻿using MacroNumpad.BusinessLogic.ViewModels.Main;
using MacroNumpad.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Unity;
using Unity.Resolution;

namespace MacroNumpad.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IMainWindowAccess
    {
        // Private constants --------------------------------------------------

        private const int SCREEN_MARGIN = 16;
        private const int TEMPORARY_SHOW_TIME_MS = 5000; // 5 seconds

        // Private fields -----------------------------------------------------

        private MainWindowViewModel viewModel;
        private DispatcherTimer timer;

        // Private methods ----------------------------------------------------

        private void OnSourceInitialized(object sender, EventArgs e)
        {
            var interopHelper = new WindowInteropHelper(this);
            int exStyle = Win32.GetWindowLong(interopHelper.Handle, Win32.GWL_EXSTYLE);
            Win32.SetWindowLong(interopHelper.Handle, Win32.GWL_EXSTYLE, exStyle | Win32.WS_EX_NOACTIVATE | Win32.WS_EX_TOPMOST);
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Hide();
        }

        private void HandleShowTimerTick(object sender, EventArgs e)
        {
            Hide();
            timer.Stop();
        }

        private void TaskbarIcon_TrayRightMouseDown(object sender, RoutedEventArgs e)
        {
            viewModel.UpdateTrayMenuItems();
        }

        // IMainWindowAccess implementation -----------------------------------

        void IMainWindowAccess.ShowAndPositionWindow(bool withTimer)
        {
            var mainScreen = System.Windows.Forms.Screen.PrimaryScreen;

            Show();

            PresentationSource source = PresentationSource.FromVisual(this);
            double dpiX = source.CompositionTarget.TransformToDevice.M11;
            double dpiY = source.CompositionTarget.TransformToDevice.M22;

            double widthPx = Width * dpiX;
            double heightPx = Height * dpiY;

            this.Left = (mainScreen.WorkingArea.Right - widthPx) / dpiX - SCREEN_MARGIN;
            this.Top = (mainScreen.WorkingArea.Bottom - heightPx) / dpiY - SCREEN_MARGIN;

            timer.Stop();

            if (withTimer)
            {
                timer.Start();
            }
        }

        void IMainWindowAccess.CloseApplication()
        {
            Application.Current.Shutdown();            
        }

        void IMainWindowAccess.RunOnMainThread(Action action)
        {
            Dispatcher.BeginInvoke(action);
        }

        // Public methods -----------------------------------------------------

        public MainWindow()
        {
            InitializeComponent();
            this.SourceInitialized += this.OnSourceInitialized;

            viewModel = Dependencies.Container.Instance.Resolve<MainWindowViewModel>(new ParameterOverride("access", this));
            DataContext = viewModel;

            timer = new DispatcherTimer(DispatcherPriority.Normal);
            timer.Interval = TimeSpan.FromMilliseconds(TEMPORARY_SHOW_TIME_MS);
            timer.Tick += HandleShowTimerTick;
        }
    }
}
