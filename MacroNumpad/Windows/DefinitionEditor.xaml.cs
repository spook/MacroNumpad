﻿using MacroNumpad.BusinessLogic.Models.Config.Definition;
using MacroNumpad.BusinessLogic.ViewModels.Definition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Unity;
using Unity.Resolution;

namespace MacroNumpad.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy DefinitionEditor.xaml
    /// </summary>
    public partial class DefinitionEditor : Window, IDefinitionEditorWindowAccess
    {
        private DefinitionEditorWindowViewModel viewModel;

        public DefinitionEditor()
        {
            InitializeComponent();

            viewModel = Dependencies.Container.Instance.Resolve<DefinitionEditorWindowViewModel>(new ParameterOverride("access", this));
            DataContext = viewModel;
        }

        public NumpadDefinition Result => viewModel.Result;

        public void Close(bool result)
        {
            DialogResult = result;
            Close();
        }
    }
}
